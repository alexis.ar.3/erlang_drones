-module(main).
-author("Alexis Armijos - Jessica Guazha - Jaime Paqui").
-import(lists,[member/2]).
-import(random,[uniform/1, uniform/0]).
-import(string,[concat/2, substr/3]).
-import(lists,[append/2]).
-import(math,[pow/2, sqrt/1]).

%% API
-export([start/0, iniciarDrone/6]).


start() ->
  Empresas = ["Central==>","Alfa==>","Beta==>","Delta==>","Tau==>","Zeta==>"],
  io:fwrite("~w~n",[member("Alfa==>",Empresas)]),
  EmpresasMapa = posicionarEmpresas(Empresas),
  io:format("~nEmpresas Posicinadas en el mapa~n~p~n",[EmpresasMapa]),
  CantidadPedidos = 5,
  Pedidos = crearPedidos(CantidadPedidos, EmpresasMapa, "", 0),
  io:format("Lista pedidos: ~p~n", [Pedidos]),
%%  PosiEmpresaBuscada = obtenerEmpresa(EmpresasMapa,"Alfa").
  organizarDrones(Pedidos).
%%  Pid = spawn(?MODULE, iniciarDrone, ["hello", "process"]),
%%  io:fwrite("~p",[Pid]).

iniciarDrone(Pedido, Xcli, Ycli, Xent, Yent, Id) ->
  Distancia = calcularDistancia(Xcli, Ycli, Xent, Yent),
  Tiempo = list_to_integer(float_to_list(calcularTiempo(Distancia), [{decimals,0}])),
  DataDrone = "Drone" ++ integer_to_list(Id) ++ " con " ++ binary_to_list(Pedido),
  Velocidad = list_to_integer(float_to_list(Distancia / Tiempo, [{decimals,0}])),
  io:format("Drone~p con ~p, Distancia: ~p Tiempo: ~p seg. ~n", [Id,binary_to_list(Pedido), Distancia, Tiempo]),
  viajeDrone(DataDrone, Xcli, Ycli, Xent, Yent, Velocidad, Distancia, Tiempo).

viajeDrone(DataDrone, Xcli, Ycli, Xent, Yent, Velocidad, Distancia, Tiempo) ->
  viajeIdaDrone(DataDrone, Xcli, Ycli, Xent, Yent, Velocidad, Distancia, Tiempo),
  io:format("~p volviendo a la base.~n",[substr(DataDrone,1,6)]),
  viajeVueltaDrone(substr(DataDrone,1,6), Xcli, Ycli, Xent, Yent, Velocidad, Distancia, Tiempo).

viajeIdaDrone(DataDrone, Xcli, Ycli, Xent, Yent, Velocidad, Distancia, Tiempo) ->
  if
    Distancia < 0 ->
      io:format("~p, llegó a su destino. Esperará 5 seg para regresar ~n",[DataDrone]),
      timer:sleep(5000);
    true ->
      if
        Distancia == 0 ->
          io:format("~p, llegó a su destino. Esperará 5 seg para regresar ~n",[DataDrone]),
          timer:sleep(5000);
        true ->
          io:format("~p, Punto de Entrega X:~p Y:~p, Distancia: ~p, Tiempo: ~p seg, Velocidad: ~p m/s~n", [DataDrone, binary_to_list(Xent), binary_to_list(Yent), Distancia, Tiempo, Velocidad]),
          timer:sleep(1000),
          viajeIdaDrone(DataDrone, Xcli, Ycli, Xent, Yent, Velocidad, Distancia - Velocidad, Tiempo - 1)
      end
  end.
viajeVueltaDrone(DataDrone, Xcli, Ycli, Xent, Yent, Velocidad, Distancia, Tiempo) ->
  if
    Distancia < 0 ->
      io:format("~p, llegó a su base. ~n",[DataDrone]);
    true ->
      if
        Distancia == 0 ->
          io:format("~p, llegó a su base.~n",[DataDrone]);
        true ->
          io:format("~p, Ubicación de la base X:~p Y:~p, Distancia: ~p, Tiempo: ~p seg, Velocidad: ~p m/s~n", [DataDrone, binary_to_list(Xcli), binary_to_list(Ycli), Distancia, Tiempo, Velocidad]),
          timer:sleep(1000),
          viajeVueltaDrone(DataDrone, Xcli, Ycli, Xent, Yent, Velocidad, Distancia - Velocidad, Tiempo - 1)
      end
  end.


prepararDrone([], _) ->
  "Drones Organizados";
prepararDrone([Primero | _Resto], Id) ->
  [Pedido, Xcli, Ycli, Xent, Yent] = re:split(Primero, "-"),
  timer:sleep(uniform(1000)),
  spawn(?MODULE, iniciarDrone, [Pedido, Xcli, Ycli, Xent, Yent, Id]),
  prepararDrone(_Resto, Id + 1).

organizarDrones(ListaPedidos) ->
  [A, B, C, D, E] = re:split(ListaPedidos, "_"),
  Lista = [A, B, C, D, E],
  prepararDrone(Lista, 1).
%%  timer:sleep(2000),
%%  io:format("Drones organizados~n").

calcularDistancia(Xcli, Ycli, Xent, Yent) ->
  X = binary_to_integer(Xent) - binary_to_integer(Xcli),
  Y = binary_to_integer(Yent) - binary_to_integer(Ycli),
  Suma = pow(X, 2) + pow(Y, 2),
  list_to_integer(float_to_list(sqrt(Suma), [{decimals,0}])).

calcularTiempo(0) ->
  0;
calcularTiempo(Distancia) ->
  0.01 + (uniform() / 1000) + calcularTiempo(Distancia - 1).

posicionarEmpresas([]) ->
  [];
posicionarEmpresas([Primera | Resto])->
  [NombreActual, _Posi] = re:split(Primera, "==>"),
  posicionarEmpresas(Resto) ++ [(binary_to_list(NombreActual) ++ "-" ++ integer_to_list(uniform(1000))) ++ "-" ++ integer_to_list(uniform(1000))].

obtenerEmpresa([], EmpresaBuscada)->
  [];
obtenerEmpresa([Primera | Resto], EmpresaBuscada) ->
  [NombreActual, _PosiX, _PosiY] = re:split(Primera, "-"),
  NombreActualEmpresa = binary_to_list(NombreActual),
  X = binary_to_integer(_PosiX),
  Y = binary_to_integer(_PosiY),
  if
    NombreActualEmpresa == EmpresaBuscada ->
      integer_to_list(X) ++ "-" ++ integer_to_list(Y);
    true ->
      obtenerEmpresa(Resto, EmpresaBuscada)
  end.

obtenerEmpresaPorPosicion([], Cont, Posi)->
  [];
obtenerEmpresaPorPosicion([Primera | Resto], Cont, Posi) ->
  [_NombreActual, _PosiX, _PosiY] = re:split(Primera, "-"),
  X = binary_to_integer(_PosiX),
  Y = binary_to_integer(_PosiY),
  if
    Cont == Posi ->
      integer_to_list(X) ++ "-" ++ integer_to_list(Y);
    true ->
      obtenerEmpresaPorPosicion(Resto, Cont+1, Posi)
  end.


crearPedidos(NumeroOrdenes, Empresas, ListaPedidos, Cont) ->

  if
    Cont < NumeroOrdenes ->
      if
         Cont == (NumeroOrdenes - 1)->
%%           io:format("Entra true ~w ~w~n", [Cont,(NumeroOrdenes-1)]),
           NewPedido = "Pedido" ++ integer_to_list(Cont + 1) ++ "-" ++ obtenerEmpresaPorPosicion(Empresas, 1, uniform(5)) ++ "-" ++ integer_to_list(uniform(1000)) ++ "-" ++ integer_to_list(uniform(1000));
        true ->
%%          io:format("Entra false ~w ~w~n", [Cont,(NumeroOrdenes-1)]),
          NewPedido = "Pedido" ++ integer_to_list(Cont + 1) ++ "-" ++ obtenerEmpresaPorPosicion(Empresas, 1, uniform(5)) ++ "-" ++ integer_to_list(uniform(1000)) ++ "-" ++ integer_to_list(uniform(1000)) ++ "_"
      end,
%%      io:format("Nuevo pedido: ~p~n", [NewPedido]),
      NewPedido ++ crearPedidos(NumeroOrdenes, Empresas, ListaPedidos, Cont + 1);
    true ->
      ListaPedidos
  end.

